package com.itheima.reggie.test;

import com.itheima.reggie.mapper.EmployeeMapper;
import com.itheima.reggie.entity.Employee;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;
import java.util.List;

@SpringBootTest
public class TestEmployeeMapper {

    @Autowired
    private EmployeeMapper employeeMapper;

    @Test
    void selectAll(){
        List<Employee> employees = employeeMapper.selectList(null);
        System.out.println(employees);
    }

    @Test
    void save(){
        Employee employee = new Employee();
        employee.setId(3L);
        employee.setName("管理员");
        employee.setUsername("kunkun");
        employee.setPassword("123");
        employee.setPhone("13655549888");
        employee.setSex("1");
        employee.setIdNumber("220201133225610086");
        employee.setStatus(1);
        employee.setCreateTime(LocalDateTime.now());
        employee.setUpdateTime(LocalDateTime.now());
        employee.setCreateUser(1L);
        employee.setUpdateUser(2L);
        employeeMapper.insert(employee);
        System.out.println(employee);
    }


}
