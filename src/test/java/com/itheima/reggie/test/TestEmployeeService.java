package com.itheima.reggie.test;

import com.itheima.reggie.entity.Employee;
import com.itheima.reggie.service.EmployeeService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class TestEmployeeService {

    @Autowired
    private EmployeeService employeeService;

    @Test
    void getById(){
        Employee id = employeeService.getById(1L);
        System.out.println(id);
    }
}
