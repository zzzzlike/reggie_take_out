package com.itheima.reggie.config;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClient;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "oss.aliyun")
@Data
public class AliyunConfig {
    private String access_key_id;
    private String access_key_secret;
    private String endpoint;
    private String bucket;

    @Bean
    public OSS oSSClient() {
        return new OSSClient(endpoint, access_key_id, access_key_secret);
    }
}