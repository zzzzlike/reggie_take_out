package com.itheima.reggie.utils;

import com.aliyun.oss.OSS;
import com.aliyun.oss.model.OSSObject;
import com.itheima.reggie.config.AliyunConfig;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

/**
 * 阿里云OSS云存储工具类
 */
@Component
@Slf4j
public class AliyunOSSUtils {

    /**
     * 图片格式
     */
    private static final String[] IMAGE_TYPE = new String[]{".bmp", ".jpg",
            ".jpeg", ".gif", ".png"};

    private static final String[] VIDEOS = new String[]{".mp4"};

    @Autowired
    private OSS ossClient;

    @Autowired
    private AliyunConfig aliyunConfig;

    @Value("${oss.aliyun.base}")
    private String base;

    /**
     * 文件上传
     *
     * @param uploadFile
     * @return
     * @throws IOException
     */
    public String UpLoadImage(MultipartFile[] uploadFile) throws IOException {

        // 校验图片格式
        boolean isLegal = false;
        log.info("----校验图片格式---");
        for (MultipartFile multipartFile : uploadFile) {
            // 校验图片格式
            for (String type : IMAGE_TYPE) {
                if (StringUtils.endsWithIgnoreCase(multipartFile.getOriginalFilename(), type)) {
                    isLegal = true;
                    break;
                }
            }
        }
        if (!isLegal) {
            log.info("----图片格式有误---");
            return null;
        }
        for (MultipartFile multipartFile : uploadFile) {
            //获取原始文件名
            String originalFilename = multipartFile.getOriginalFilename();
            //截取文件后缀名
            String suffix = originalFilename.substring(originalFilename.indexOf("."));//.JEPG
            //使用UUID拼接后缀名生成新的文件名防止重复(拼接base文件夹名称)
            String fileName = UUID.randomUUID().toString() + suffix; //   /IMG/1c5f2e3b4.JEPG

            try {
                //核心方法
                ossClient.putObject(aliyunConfig.getBucket(), base+fileName, new
                        ByteArrayInputStream(multipartFile.getBytes()));

                log.info("--------图片上传成功--------");
                //返回文件名
                return fileName;
            } catch (Exception e) {
                e.printStackTrace();
                log.info("上传失败");
            }
        }
        return null;
    }

    /**
     * 文件下载
     *
     * @param name
     * @param response
     * @return
     */
    public Boolean DownLoadImage(String name, HttpServletResponse response) {
        log.info("文件名称:{}", name);
        try {
            // 调用ossClient.getObject返回一个OSSObject实例，该实例包含文件内容及文件元信息。
            OSSObject ossObject = ossClient.getObject(aliyunConfig.getBucket(), base + name);
            // 调用ossObject.getObjectContent获取文件输入流，可读取此输入流获取其内容。
            InputStream is = ossObject.getObjectContent();

            ServletOutputStream ops = response.getOutputStream();

            try {
                //使用对拷工具进行对拷,给浏览器写回文件
                IOUtils.copy(is, ops);

                //关流
                // 数据读取完成后，获取的流必须关闭，否则会造成连接泄漏，导致请求无连接可用，程序无法正常工作。
                is.close();
                ops.close();

            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
            log.info("----图片下载成功---");
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            log.info("下载失败");
            return false;
        }
    }
}