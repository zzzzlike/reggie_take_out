package com.itheima.reggie.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.reggie.entity.Category;

import java.util.List;

public interface CategoryService extends IService<Category> {

    /**
     * 分类分页查询
     * @param page
     * @param pageSize
     * @return
     */
    Page<Category> selectByPage(int page,int pageSize);

    /**
     * 根据id删除分类
     * @param id
     * @return
     */
    boolean deleteById(Long id);

    /**
     * 根据type查询所有分类
     * @param category
     * @return
     */
    List<Category> getByType(Category category);
}
