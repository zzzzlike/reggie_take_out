package com.itheima.reggie.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.reggie.entity.Orders;

import java.util.Map;

public interface OrderService extends IService<Orders> {

    /**
     *
     * @param orders
     */
    void submitOrders(Orders orders);

    /**
     * 分页查询订单
     * @param page
     * @param pageSize
     * @return
     */
    Page selectPage(int page,int pageSize);

    /**
     * 根据条件查询订单
     * @param map
     * @return
     */
    Page OrdersByCondition(Map map);

}
