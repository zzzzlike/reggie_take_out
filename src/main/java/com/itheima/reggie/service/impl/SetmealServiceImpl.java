package com.itheima.reggie.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.reggie.common.CustomException;
import com.itheima.reggie.dto.SetmealDto;
import com.itheima.reggie.entity.Category;
import com.itheima.reggie.entity.Setmeal;
import com.itheima.reggie.entity.SetmealDish;
import com.itheima.reggie.mapper.SetmealMapper;
import com.itheima.reggie.service.CategoryService;
import com.itheima.reggie.service.SetmealDishService;
import com.itheima.reggie.service.SetmealService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class SetmealServiceImpl extends ServiceImpl<SetmealMapper, Setmeal> implements SetmealService {

    @Autowired
    SetmealDishService setmealDishService;

    @Autowired
    CategoryService categoryService;

    /**
     * 新增套餐
     * @param setmealDto
     */
    @Override
    public void addSetmeal(SetmealDto setmealDto) {
        //保存套餐数据到 setmeal表中
        this.save(setmealDto);

        //保存套餐菜品数据到 setmeal_dish表中： SetmealDishMapper
        //获取当前套餐的菜品列表
        List<SetmealDish> setmealDishes = setmealDto.getSetmealDishes();
        //循环遍历，获取每一个套餐菜品数据 setmealDish
        //获取套餐id，并set到setmealDish中
        setmealDishes = setmealDishes.stream().map(item ->{
            item.setSetmealId(setmealDto.getId());
            return item;
        }).collect(Collectors.toList());

        //保存套餐菜品数据 setmealDish
        setmealDishService.saveBatch(setmealDishes);
    }

    /**
     * 套餐分页查询
     * @param page
     * @param pageSize
     * @param name
     * @return
     */
    @Override
    public Page selectPage(int page, int pageSize, String name) {
        //构造分页构造器
        Page<Setmeal> pageInfo = new Page<>(page,pageSize);
        Page<SetmealDto> dtoPage = new Page<>();

        //构造查询
        LambdaQueryWrapper<Setmeal> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.like(name != null,Setmeal::getName,name);
        queryWrapper.orderByDesc(Setmeal::getUpdateTime);

        Page<Setmeal> setmealPage = this.page(pageInfo, queryWrapper);
        //数据对拷
        BeanUtils.copyProperties(pageInfo,dtoPage,"records");

        //处理原始的records数据
        List<Setmeal> records = pageInfo.getRecords();

        //item依次代表每个records
        List<SetmealDto> setmealList = records.stream().map(item -> {
            SetmealDto setmealDto = new SetmealDto();
            //数据对拷
            BeanUtils.copyProperties(item,setmealDto);
            //获取套餐分类id
            Long categoryId = setmealDto.getCategoryId();
            //根据分类ID获取category对象
            Category category = categoryService.getById(categoryId);
            if (category != null) {
                //把对应的分类名称赋值给SetmealDto
                setmealDto.setCategoryName(category.getName());
            }
            return setmealDto;
        }).collect(Collectors.toList());

        dtoPage.setRecords(setmealList);

        return dtoPage;

    }

    /**
     * 删除套餐
     * @param ids
     */
    @Override
    public void deleteSetmeal(List<Long> ids) {
        //1、判断当前要删除的套餐是否处于售卖状态，如果是，不允许删除
        // select count(*) from setmeal where id in (1,2,3) and status = 1
        LambdaQueryWrapper<Setmeal> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Setmeal::getStatus,1).in(Setmeal::getId,ids);
        //2、删除套餐：   逻辑删除
        int count = this.count(queryWrapper);
        if (count > 0){
            throw new CustomException("套餐未停售,不能删除");
        }
        this.removeByIds(ids);
        //3、删除套餐对应的菜品数据：  物理删除
        LambdaUpdateWrapper<SetmealDish> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.in(SetmealDish::getSetmealId,ids);

        setmealDishService.remove(updateWrapper);

    }

    /**
     * 起售/停售套餐
     * @param status
     * @param ids
     */
    @Override
    public void updateStatus(Integer status, List<Long> ids) {
        //构造查询
        LambdaUpdateWrapper<Setmeal> updateWrapper = new LambdaUpdateWrapper<>();

        //根据查询到的id修改符合条件的状态
        updateWrapper.set(Setmeal::getStatus,status).in(Setmeal::getId,ids);
        this.update(updateWrapper);
    }

    /**
     * 根据套餐id查询套餐数据（回显数据）
     * @param id
     * @return
     */

    @Override
    public SetmealDto getSetmealId(Long id) {
        Setmeal setmeal = super.getById(id);

        SetmealDto setmealDto = new SetmealDto();

        //BeanUtils.copyProperties() 拷贝setmeal到setmealDto中
        BeanUtils.copyProperties(setmeal,setmealDto);
        //构造查询
        //根据套餐id查询套餐对应的菜品数据 select * from setmeal_dish where setmeal_id = ?
        LambdaQueryWrapper<SetmealDish> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(SetmealDish::getSetmealId,setmealDto.getId());
        //将套餐 setmeal 和套餐菜品集合 setmealDishList 数据封装到 SetmealDto并返回
        List<SetmealDish> setmealDishes = setmealDishService.list(queryWrapper);
        //将查询的菜品放到setmealDto中
        setmealDto.setSetmealDishes(setmealDishes);
        //返回 setmealDto
        return setmealDto;
    }

    /**
     * 修改菜品
     * @param setmealDto
     */
    @Override
    public void updateSetmeal(SetmealDto setmealDto) {
        this.updateById(setmealDto);
        //构造查询对应的菜品数据
        LambdaQueryWrapper<SetmealDish> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(SetmealDish::getSetmealId,setmealDto.getId());
        //删除菜品数据
        setmealDishService.remove(queryWrapper);
        //修改后的套餐菜品添加到关系表中
        List<SetmealDish> setmealDishes = setmealDto.getSetmealDishes();
        //遍历集合用stream流赋值setmealDish到每一个id
        setmealDishes = setmealDishes.stream().map(item -> {
            item.setSetmealId(setmealDto.getId());
            return item;
        }).collect(Collectors.toList());

        //获得的套餐菜品保存到关系表中
        setmealDishService.saveBatch(setmealDishes);
    }

    /**
     * 根据套餐分类id，查询对应套餐数据
     * @param setmeal
     * @return
     */
    @Override
    public List<Setmeal> selectSetmeal(Setmeal setmeal) {
        //根据套餐分类id查询对应的套餐集合数据
        LambdaQueryWrapper<Setmeal> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(setmeal.getCategoryId() != null,Setmeal::getCategoryId,setmeal.getCategoryId());
        //查询状态
        queryWrapper.eq(setmeal.getStatus() != null,Setmeal::getStatus,setmeal.getStatus());

        //排序
        queryWrapper.orderByDesc(Setmeal::getUpdateTime);

        List<Setmeal> setmealList = this.list(queryWrapper);

        return setmealList;

    }

}