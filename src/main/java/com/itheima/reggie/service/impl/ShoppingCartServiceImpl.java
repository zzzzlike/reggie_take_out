package com.itheima.reggie.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.reggie.common.BaseContext;
import com.itheima.reggie.entity.ShoppingCart;
import com.itheima.reggie.mapper.ShoppingCartMapper;
import com.itheima.reggie.service.ShoppingCartService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class ShoppingCartServiceImpl extends ServiceImpl<ShoppingCartMapper, ShoppingCart> implements ShoppingCartService {

    @Autowired
    ShoppingCartMapper shoppingCartMapper;

    /**
     * 添加购物车
     * @param shoppingCart
     * @return
     */
    @Override
    public ShoppingCart addShoppingCart(ShoppingCart shoppingCart) {
        // 获取用户id
        Long userId = BaseContext.getCurrentId();
        //设置用户id
        shoppingCart.setUserId(userId);

        /* 菜品或者套餐是否点过
        如何知道烧鹅被点过了？？ 根据烧鹅的菜品id，查询下数据库中有没有烧鹅这个数据

        如何是菜品：select * from shopping_cart where dish_id = ? and user_id = ?
        如果是套餐：select * from shopping_cart where setmeal_id = ? and user_id = ?
        */
        LambdaUpdateWrapper<ShoppingCart> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.eq(ShoppingCart::getUserId,userId);

        //判断当时用户点的是菜品还是套餐
        Long dishId = shoppingCart.getDishId();
        if (dishId != null){//说明点的是菜品
            updateWrapper.eq(ShoppingCart::getDishId,dishId);
        }else {//如果不是菜品，那么一定是套餐
            updateWrapper.eq(ShoppingCart::getSetmealId,shoppingCart.getSetmealId());
        }

        ShoppingCart one = shoppingCartMapper.selectOne(updateWrapper);
        // 第一次点的时候是新增 ，后面在点击的时候是修改
        if (one == null){
            //如果没有则是新增
            //设置 number=1
            shoppingCart.setNumber(1);
            shoppingCart.setUserId(userId);
            //存储到购物车表中
            shoppingCartMapper.insert(shoppingCart);
            one = shoppingCart;
        }else {
            // 如果有，则更新
            one.setNumber(one.getNumber()+1);
            shoppingCartMapper.updateById(one);
        }
        return one;
    }

    /**
     * 查询购物车
     * @return
     */
    @Override
    public List<ShoppingCart> selectShoppingCart() {
        log.info("查看购物车...");
        //构造查询
        LambdaQueryWrapper<ShoppingCart> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(ShoppingCart::getUserId, BaseContext.getCurrentId());
        queryWrapper.orderByDesc(ShoppingCart::getCreateTime);

        List<ShoppingCart> shoppingCarts = shoppingCartMapper.selectList(queryWrapper);
        return shoppingCarts;
    }

    /**
     * 取消购物车
     * @param shoppingCart
     * @return
     */
    @Override
    public ShoppingCart subShoppingCart(ShoppingCart shoppingCart) {
        // 获取用户id
        Long userId = BaseContext.getCurrentId();
        //设置用户id
        shoppingCart.setUserId(userId);
        //菜品或者套餐id 查询出当前ShoppingCart数据
        // 如何是菜品：select * from shopping_cart where dish_id = ? and user_id = ?
        // 如果是套餐：select * from shopping_cart where setmeal_id = ? and user_id = ?
        LambdaQueryWrapper<ShoppingCart> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(ShoppingCart::getUserId,userId);//where user_id =?

        //判断当时用户点的是菜品还是套餐
        Long dishId = shoppingCart.getDishId();
        if (dishId != null){//是菜品
            queryWrapper.eq(ShoppingCart::getDishId,dishId);
        }else {//是套餐
            queryWrapper.eq(ShoppingCart::getSetmealId,shoppingCart.getSetmealId());//setmeal_id = ?
        }

        //根据查询的结果进行不同的操作:查询到菜品/套餐的id
        //判断查询到的购物车数据number是否为1
        ShoppingCart one = shoppingCartMapper.selectOne(queryWrapper);
        Integer number = one.getNumber();
        number --;
        one.setNumber(number);
        if (number > 0){
            shoppingCartMapper.updateById(one);
        }else {
            shoppingCartMapper.delete(queryWrapper);
        }

        return one;
    }

    /**
     * 清空购物车
     */
    @Override
    public void deleteShoppingCart() {
        // 根据当前用户id进行删除：delete from shoping_cart where user_id = ?
        LambdaQueryWrapper<ShoppingCart> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(ShoppingCart::getUserId,BaseContext.getCurrentId());
        log.info("清空购物车...");
        shoppingCartMapper.delete(queryWrapper);
    }


}
