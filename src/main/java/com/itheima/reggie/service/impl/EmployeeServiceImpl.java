package com.itheima.reggie.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.reggie.common.R;
import com.itheima.reggie.entity.Employee;
import com.itheima.reggie.mapper.EmployeeMapper;
import com.itheima.reggie.service.EmployeeService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

@Service
public class EmployeeServiceImpl extends ServiceImpl<EmployeeMapper, Employee> implements EmployeeService {

    @Autowired
    EmployeeMapper employeeMapper;

    /**
     * 判断员工用户名登录
     * @param employee
     * @return
     */
    @Override
    public Map checkLogin(Employee employee) {
        Map resultMap = new HashMap();
        LambdaQueryWrapper<Employee> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Employee::getUsername,employee.getUsername());
        Employee employeeOne = employeeMapper.selectOne(queryWrapper);

        //判断用户是否存在
        if (employeeOne == null){
            resultMap.put("msg","用户不存在");
            return resultMap;
        }

        //md5加密
        String digestAsHex = DigestUtils.md5DigestAsHex(employee.getPassword().getBytes());
        //判断密码是否正确
        if (!digestAsHex.equals(employeeOne.getPassword())){
            resultMap.put("msg","密码不正确！");
            return resultMap;
        }
        //判断账号是否被禁用
        if (employeeOne.getStatus() == 0){
            resultMap.put("msg","账号被禁用");
            return resultMap;
        }
        //登录成功
        resultMap.put("employee",employeeOne);
        return resultMap;
    }


//    /**
//     * 员工新增(公共字段填充)
//     * @param id
//     * @param employee
//     * @return
//     */
//    @Override
//    public boolean save(Long id,Employee employee) {
//
//        //create_time 和 update_time ,设置为当前系统时间  LocalDateTime.now()
//        employee.setCreateTime(LocalDateTime.now());
//        employee.setUpdateTime(LocalDateTime.now());
//
//        //create_user 和 update_user 创建人和修改人，应该是当前登陆人，从session中获取
//        employee.setCreateUser(id);
//        employee.setUpdateUser(id);
//
//        //设置初始密码为123456,用md5加密处理
//        employee.setPassword(DigestUtils.md5DigestAsHex("123456".getBytes()));
//        boolean flag = employeeMapper.insert(employee) > 0;
//
//        return flag;
//    }

    /**
     * 员工分页查询
     * @param page
     * @param pageSize
     * @param name
     * @return
     */
    @Override
    public Page<Employee> selectByPage(int page, int pageSize, String name) {
        //添加分页构造器
        Page pageInfo = new Page(page, pageSize);
        //构造条件
        LambdaQueryWrapper<Employee> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.like(StringUtils.isNotEmpty(name),Employee::getName,name);
        queryWrapper.orderByAsc(Employee::getCreateTime);

        employeeMapper.selectPage(pageInfo,queryWrapper);
        return pageInfo;

    }
    
}
