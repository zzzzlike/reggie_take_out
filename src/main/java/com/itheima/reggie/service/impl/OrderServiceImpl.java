package com.itheima.reggie.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.reggie.common.BaseContext;
import com.itheima.reggie.dto.OrdersDto;
import com.itheima.reggie.entity.*;
import com.itheima.reggie.mapper.*;
import com.itheima.reggie.service.OrderDetailService;
import com.itheima.reggie.service.OrderService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class OrderServiceImpl extends ServiceImpl<OrderMapper, Orders> implements OrderService {

    @Autowired
    OrderMapper orderMapper;

    @Autowired
    ShoppingCartMapper shoppingCartMapper;

    @Autowired
    AddressBookMapper addressBookMapper;

    @Autowired
    UserMapper userMapper;

    @Autowired
    OrderDetailMapper orderDetailMapper;

    @Autowired
    OrderDetailService orderDetailService;

    /**
     * 下单
     * @param orders
     */
    @Override
    public void submitOrders(Orders orders) {
        // orders  order_detail
        // 0、获取用户id
        Long userId = BaseContext.getCurrentId();
        // 1、查询购物车数据：select * from shopping_cart where user_id = ?
        LambdaQueryWrapper<ShoppingCart> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(ShoppingCart::getUserId,userId);
        List<ShoppingCart> shoppingCarts = shoppingCartMapper.selectList(queryWrapper);

        // 2、查询用户数据 select * from user where id = ?
        User user = userMapper.selectById(userId);

        // 3、根据地址id查询地址数据 select * from address_book where id = ?
        AddressBook addressBook = addressBookMapper.selectById(orders.getAddressBookId());

        long orderId = IdWorker.getId();//订单号

        BigDecimal amount = new BigDecimal(0);//总价
        // 4、保存订单明细数据：点了哪些菜品或套餐
        for (ShoppingCart shoppingCart : shoppingCarts) {
            // ShoppingCart shoppingCart --> OrderDetail orderDetail
            OrderDetail orderDetail = new OrderDetail();

            BeanUtils.copyProperties(shoppingCart,orderDetail);
            // orderId单独设置
            orderDetail.setOrderId(orderId);

            // amount 金额单独设置 数量  *  单价
            BigDecimal price = shoppingCart.getAmount();
            BigDecimal number = new BigDecimal(shoppingCart.getNumber());
            orderDetail.setAmount(price.multiply(number));
            amount.add(orderDetail.getAmount());
            orderDetailMapper.insert(orderDetail);
        }
        // 5、保存订单数据，有许多数据需要单独设置
        orders.setId(orderId);//订单id
        orders.setOrderTime(LocalDateTime.now());//订单创建时间
        orders.setCheckoutTime(LocalDateTime.now());//订单结账时间
        orders.setStatus(2);//1待付款，2待派送，3已派送，4已完成，5已取消
        orders.setAmount(amount);
        orders.setUserId(userId);
        orders.setNumber(String.valueOf(orderId));
        orders.setUserName(user.getName());
        orders.setConsignee(addressBook.getConsignee());
        orders.setPhone(addressBook.getPhone());
        orders.setAddress(
                (addressBook.getProvinceName() == null ? "" : addressBook.getProvinceName())
                        + (addressBook.getCityName() == null ? "" : addressBook.getCityName())
                        + (addressBook.getDistrictName() == null ? "" : addressBook.getDistrictName())
                        + (addressBook.getDetail() == null ? "" : addressBook.getDetail()));//省+市+区域+详细地址
        orderMapper.insert(orders);

        // 6、删除当前用户的购物车数据
        // delete from shopping_cart where user_id = ?
        LambdaQueryWrapper<ShoppingCart> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(ShoppingCart::getUserId,userId);
        shoppingCartMapper.delete(wrapper);
    }

    /**
     * 分页查询订单
     * @param page
     * @param pageSize
     * @return
     */
    @Override
    public Page selectPage(int page, int pageSize) {
        //分页构造器
        Page<Orders> pageInfo = new Page<>(page, pageSize);
        Page<OrdersDto> dtoPage = new Page<>();

        //构造订单查询
        LambdaQueryWrapper<Orders> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.orderByDesc(Orders::getOrderTime);
        orderMapper.selectPage(pageInfo,queryWrapper);

        //分页数据对拷给dto(不处理records)
        BeanUtils.copyProperties(pageInfo,dtoPage,"records");
        //处理records
        List<Orders> records = pageInfo.getRecords();
        List<OrdersDto> ordersDtos = records.stream().map(item -> {
            OrdersDto ordersDto = new OrdersDto();
            //对拷数据,records中的订单数据给dto
            BeanUtils.copyProperties(item,ordersDto);
            //构造查询orderId对应的订单数据
            LambdaQueryWrapper<OrderDetail> wrapper = new LambdaQueryWrapper<>();
            wrapper.eq(OrderDetail::getOrderId,item.getId());
            List<OrderDetail> orderDetails = orderDetailService.list(wrapper);
            //订单数据赋值给ordersDto
            ordersDto.setOrderDetails(orderDetails);
            return ordersDto;
        }).collect(Collectors.toList());
        //处理完的records数据赋值给dtoPage
        dtoPage.setRecords(ordersDtos);

        return dtoPage;
    }

    /**
     * 根据条件查询订单
     * @param map
     * @return
     */
    @Override
    public Page OrdersByCondition(Map map) {
        //map中获得具体的条件参数
        int page = Integer.parseInt((String) map.get("page"));
        int pageSize = Integer.parseInt((String) map.get("pageSize"));
        String number = (String) map.get("number");
        Object beginTimeStr = map.get("beginTime");
        Object endTimeStr = map.get("endTime");

        //将beginTime和endTime转换为LocalDateTime
        LocalDateTime beginTime = null;
        LocalDateTime endTime = null;

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        if (beginTimeStr != null && beginTimeStr != ""){
            LocalDateTime.parse(beginTimeStr.toString(),dtf);
        }
        if (endTimeStr != null && endTimeStr != ""){
            LocalDateTime.parse(endTimeStr.toString(),dtf);
        }

        //分页构造
        Page<Orders> pageInfo = new Page<>(page,pageSize);

        LambdaQueryWrapper<Orders> queryWrapper = new LambdaQueryWrapper<>();
        //下单时间大于开始时间,小于结束时间
        queryWrapper.ge(beginTime != null,Orders::getOrderTime,beginTime)
                    .le(endTime != null,Orders::getOrderTime,endTime);
        //订单号查询条件
        queryWrapper.eq(number != null,Orders::getNumber,number);
        queryWrapper.orderByDesc(Orders::getOrderTime);

        Page<Orders> ordersPage = orderMapper.selectPage(pageInfo, queryWrapper);
        return pageInfo;
    }


}
