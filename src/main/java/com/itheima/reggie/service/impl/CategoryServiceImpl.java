package com.itheima.reggie.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.reggie.common.CustomException;
import com.itheima.reggie.entity.Category;
import com.itheima.reggie.entity.Dish;
import com.itheima.reggie.entity.Setmeal;
import com.itheima.reggie.mapper.CategoryMapper;
import com.itheima.reggie.service.CategoryService;

import com.itheima.reggie.service.DishService;
import com.itheima.reggie.service.SetmealService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImpl extends ServiceImpl<CategoryMapper, Category> implements CategoryService {

    @Autowired
    CategoryMapper categoryMapper;

    @Autowired
    DishService dishService;

    @Autowired
    SetmealService setmealService;

    /**
     * 分类分页查询
     * @param page
     * @param pageSize
     * @return
     */
    @Override
    public Page<Category> selectByPage(int page, int pageSize) {
        //添加分页构造器
        Page pageInfo = new Page(page,pageSize);
        //构造条件，根据sort排升序
        LambdaQueryWrapper<Category> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.orderByAsc(Category::getSort);

        categoryMapper.selectPage(pageInfo,queryWrapper);
        return pageInfo;
    }

    /**
     * 根据id删除分类
     * @param id
     * @return
     */
    @Override
    public boolean deleteById(Long id) {
        //检查当前分类下是否关联了菜品
        LambdaQueryWrapper<Dish> dishLambdaQueryWrapper = new LambdaQueryWrapper<>();
        dishLambdaQueryWrapper.eq(Dish::getCategoryId,id);

        int dishCount = dishService.count(dishLambdaQueryWrapper);
        //判断当前分类下是否关联了菜品
        if (dishCount > 0){
            throw new CustomException("当前分类下关联了菜品,不可以直接删除！");
        }

        //检查当前分类下是否关联了套餐
        LambdaQueryWrapper<Setmeal> setmealLambdaQueryWrapper = new LambdaQueryWrapper<>();
        setmealLambdaQueryWrapper.eq(Setmeal::getCategoryId,id);

        int setmealCount = setmealService.count(setmealLambdaQueryWrapper);
        //判断当前分类下是否关联了套餐
        if (setmealCount > 0){
            throw new CustomException("当前分类下关联了套餐,不可以直接删除！");
        }

        //如果没有关联菜品或者套餐可以删除
        boolean flag = super.removeById(id);

        return flag;
    }

    /**
     * 根据type查询所有分类
     * @param category
     * @return
     */
    @Override
    public List<Category> getByType(Category category) {
        //构造条件查询
        LambdaQueryWrapper<Category> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(category.getType() != null,Category::getType,category.getType());
        //排序
        queryWrapper.orderByAsc(Category::getSort).orderByDesc(Category::getUpdateTime);

        List<Category> list = super.list(queryWrapper);
        return list;
    }

}
