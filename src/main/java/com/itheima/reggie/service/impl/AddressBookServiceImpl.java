package com.itheima.reggie.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.reggie.common.BaseContext;
import com.itheima.reggie.entity.AddressBook;
import com.itheima.reggie.mapper.AddressBookMapper;
import com.itheima.reggie.service.AddressBookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AddressBookServiceImpl extends ServiceImpl<AddressBookMapper, AddressBook> implements AddressBookService {

    @Autowired
    private AddressBookMapper addressBookMapper;

    /**
     * 新增地址
     *
     * @param addressBook
     */
    @Override
    public void saveAddress(AddressBook addressBook) {
        //设置改地址对应的用户id
        addressBook.setUserId(BaseContext.getCurrentId());
        //添加地址信息到地址表里
        addressBookMapper.insert(addressBook);
    }

    /**
     * 查询指定用户的全部地址
     *
     * @return
     */
    @Override
    public List<AddressBook> getAllAddress() {
        //构造查询
        LambdaQueryWrapper<AddressBook> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(AddressBook::getUserId, BaseContext.getCurrentId());
        //排序
        queryWrapper.orderByDesc(AddressBook::getUpdateTime);

        List<AddressBook> addressBooks = addressBookMapper.selectList(queryWrapper);
        return addressBooks;
    }

    /**
     * 设置默认地址
     *
     * @param addressBook
     */
    @Override
    public void updateDefault(AddressBook addressBook) {
        //构造查询
        LambdaUpdateWrapper<AddressBook> updateWrapper = new LambdaUpdateWrapper<>();
        //设置其他地址为非默认地址
        updateWrapper.set(AddressBook::getIsDefault, 0);

        updateWrapper.eq(AddressBook::getUserId, BaseContext.getCurrentId());
        addressBookMapper.update(addressBook, updateWrapper);
        //设置该地址为默认地址
        addressBook.setIsDefault(1);
        addressBookMapper.updateById(addressBook);
    }

    /**
     * 根据ID查询地址
     *
     * @param id
     * @return
     */
    @Override
    public AddressBook selectAddressById(Long id) {
        AddressBook addressBook = addressBookMapper.selectById(id);
        return addressBook;
    }

    /**
     * 根据当前用户id查询默认地址
     *
     * @return
     */
    @Override
    public AddressBook getDefaultAddressById() {
        //构造查询，状态为1的为默认地址

        LambdaQueryWrapper<AddressBook> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(AddressBook::getUserId, BaseContext.getCurrentId())
                .eq(AddressBook::getIsDefault, 1);

        AddressBook addressBook = addressBookMapper.selectOne(queryWrapper);
        return addressBook;

    }

    /**
     * 批量（单个）删除地址
     *
     * @param ids
     */
    @Override
    public void deleteAddress(List<Long> ids) {
        addressBookMapper.deleteBatchIds(ids);
    }

    /**
     * 修改地址
     *
     * @param addressBook
     */
    @Override
    public void updateAddress(AddressBook addressBook) {
        addressBookMapper.updateById(addressBook);
    }

}
