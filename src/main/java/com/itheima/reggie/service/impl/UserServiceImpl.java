package com.itheima.reggie.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.reggie.entity.User;
import com.itheima.reggie.mapper.UserMapper;
import com.itheima.reggie.service.UserService;
import com.itheima.reggie.utils.SMSUtils;
import com.itheima.reggie.utils.ValidateCodeUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
@Slf4j
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

    @Autowired
    UserMapper userMapper;

    /**
     * 发送手机短信验证码
     * @param user
     * @return
     */
    @Override
    public Map sendPhoneMsg(User user) {
        Map map = new HashMap();
        //获取用户手机号
        String phone = user.getPhone();
        if (StringUtils.isNotEmpty(phone)){
            //生成随机的4位验证码
            String code = ValidateCodeUtils.generateValidateCode(4).toString();
            log.info("code={}",code);

            //调用阿里云提供的短信服务API完成发送短信
            //SMSUtils.sendMessage("瑞吉外卖","",phone,code);
            SMSUtils.sendMessage("测试专用模板", "SMS_154950909", phone,code);

            //将phone和code封装到codeMap中然后通过hashmap返回
            Map codeMap = new HashMap();
            codeMap.put("phone",phone);
            codeMap.put("code",code);

            map.put("codeMap",codeMap);
        }
        map.put("msg","短信发送失败");
        return map;
    }

    /**
     * 用户输入验证码,进行登录
     * @param phone
     * @param code
     * @param codeSession
     * @return
     */
    @Override
    public Map userLogin(String phone, String code, String codeSession) {
        Map map = new HashMap();
        //判断用户输入的验证码和session中存储的code是否一致
        if (codeSession != null && codeSession.equals(code)){
            //如果一致判断是否存在
            //构造查询
            LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<>();
            queryWrapper.eq(User::getPhone,phone);

            User user = this.getOne(queryWrapper);
            //判断是否为新用户
            if (user == null){
                User newUser = new User();
                newUser.setPhone(phone);
                //设置状态为1
                newUser.setStatus(1);
                //添加新用户
                userMapper.insert(newUser);
            }
            map.put("user",user);
        }
        map.put("msg","登录失败");

        return map;
    }
}
