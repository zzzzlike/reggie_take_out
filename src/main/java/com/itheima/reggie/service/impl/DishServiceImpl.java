package com.itheima.reggie.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.reggie.common.CustomException;
import com.itheima.reggie.dto.DishDto;
import com.itheima.reggie.entity.Category;
import com.itheima.reggie.entity.Dish;
import com.itheima.reggie.entity.DishFlavor;
import com.itheima.reggie.mapper.DishMapper;
import com.itheima.reggie.service.CategoryService;
import com.itheima.reggie.service.DishFlavorService;
import com.itheima.reggie.service.DishService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class DishServiceImpl extends ServiceImpl<DishMapper, Dish> implements DishService {

    @Autowired
    DishFlavorService dishFlavorService;

    @Autowired
    CategoryService categoryService;


    /**
     * 添加菜品数据
     * @param dishDto
     * @return
     */
    @Override
    public boolean saveWithFlavor(DishDto dishDto) {
        log.info("dishDto:{}",dishDto.toString());
        //保存菜品数据
        super.save(dishDto);
        Long dishDtoId = dishDto.getId();
        //获取菜品口味数据
        List<DishFlavor> flavors = dishDto.getFlavors();

        //stream遍历flavor对象并赋值对应菜品id
        flavors = flavors.stream().map(item ->{
            item.setDishId(dishDtoId);
            return item;
        }).collect(Collectors.toList());

        //批量保存菜品口味数据
        boolean flag = dishFlavorService.saveBatch(flavors);
        return flag ;

    }

    /**
     * 分页查询菜品数据
     * @param page
     * @param pageSize
     * @param name
     * @return
     */
    @Override
    public Page selectPage(int page, int pageSize, String name) {
        //添加分页构造器
        Page<Dish> pageInfo = new Page(page,pageSize);
        Page<DishDto> dtoPage = new Page<>();
        //构造条件并排序
        LambdaQueryWrapper<Dish> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.like(name != null,Dish::getName,name);
        queryWrapper.orderByAsc(Dish::getSort).orderByDesc(Dish::getUpdateTime);
        //查询数据
        super.page(pageInfo,queryWrapper);

        //数据对拷,处理records数据
        BeanUtils.copyProperties(pageInfo,dtoPage,"records");
        List<Dish> records = pageInfo.getRecords();

        //stream流处理records
        List<DishDto> list = records.stream().map(dish ->{
            DishDto dishDto = new DishDto();
            BeanUtils.copyProperties(dish,dishDto);
            Long categoryId = dish.getCategoryId();
            Category category = categoryService.getById(categoryId);
            if (category != null){
                String categoryName = category.getName();
                dishDto.setCategoryName(categoryName);
            }
            return dishDto;
        }).collect(Collectors.toList());

        //将处理后的数据放到dtoPage中
        dtoPage.setRecords(list);

        return dtoPage;

    }

    /**
     * 根据id查询菜品（回显数据）
     * @param id
     * @return
     */
    @Override
    public DishDto selectById(Long id) {
        Dish dish = super.getById(id);

        DishDto dishDto = new DishDto();
        //数据对拷 dish -> dishDto
        BeanUtils.copyProperties(dish,dishDto);

        //构造查询
        //根据id查询对应的口味
        LambdaQueryWrapper<DishFlavor> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(DishFlavor::getDishId,dishDto.getId());

        List<DishFlavor> flavors = dishFlavorService.list(queryWrapper);
        //将查询的口味放到dishDto中
        dishDto.setFlavors(flavors);

        return dishDto;
    }

    /**
     * 修改菜品数据
     * @param dishDto
     * @return
     */
    @Override
    public boolean updateDish(DishDto dishDto) {

        super.updateById(dishDto);
        //构造查询原来的菜品口味数据
        LambdaQueryWrapper<DishFlavor> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(DishFlavor::getDishId,dishDto.getId());
        //删除口味数据
        dishFlavorService.remove(queryWrapper);

        //获取口味对象的集合
        List<DishFlavor> flavors = dishDto.getFlavors();
        //遍历集合用stream流赋值dish到每一个id
        flavors = flavors.stream().map(dish ->{
            dish.setDishId(dishDto.getId());
            return dish;
        }).collect(Collectors.toList());

        //保存数据到对应数据的集合
        boolean flag = dishFlavorService.saveBatch(flavors);

        return flag;
    }

    /**
     * 批量删除
     * 1：删除  ， 0 ： 不删除
     * @param ids
     */
    @Override
    public void deleteDish(List<Long> ids) {
        //构造查询菜品状态
        LambdaQueryWrapper<Dish> dishQueryWrapper = new LambdaQueryWrapper<>();
        dishQueryWrapper.eq(Dish::getStatus,1).in(Dish::getId,ids);

        int count = this.count(dishQueryWrapper);
        //判断菜品状态是否为1,为1删除
        if (count > 0){
            throw new CustomException("菜品未停售,不能删除！");
        }

        //逻辑删除对应的菜品
        this.removeByIds(ids);

        //找出菜品对应的口味数据并逻辑删除
        LambdaQueryWrapper<DishFlavor> flavorQueryWrapper = new LambdaQueryWrapper<>();
        flavorQueryWrapper.in(DishFlavor::getDishId,ids);

        dishFlavorService.remove(flavorQueryWrapper);

    }

    /**
     * 起售/停售  批量起售/批量停售
     * @param status
     * @param ids
     */
    @Override
    public void updateStatus(Integer status, List<Long> ids) {

        //构造查询
        LambdaUpdateWrapper<Dish> updateWrapper = new LambdaUpdateWrapper<>();

        //根据查询到的id修改符合条件的状态
        updateWrapper.set(Dish::getStatus,status).in(Dish::getId,ids);
        this.update(updateWrapper);

    }

    /**
     * 根据菜品分类id,查询对应菜品数据
     * @param dish
     * @return
     */
    @Cacheable(value = "dish_cache",key = "'dish_'+#dish.categoryId")
    @Override
    // List<Dish>   ---> List<DishDto>
    public List<DishDto> selectDishList(Dish dish) {
        //根据菜品分类id查询对应的菜品集合数据
        LambdaQueryWrapper<Dish> dishQueryWrapper = new LambdaQueryWrapper<>();
        dishQueryWrapper.eq(dish.getCategoryId() != null,Dish::getCategoryId,dish.getCategoryId());
        //菜品状态为1
        dishQueryWrapper.eq(Dish::getStatus,1);
        dishQueryWrapper.orderByDesc(Dish::getUpdateTime);

        List<Dish> dishList = this.list(dishQueryWrapper);

        List<DishDto> dishDtoList = dishList.stream().map(item -> {
            DishDto dishDto = new DishDto();
            //数据对拷
            BeanUtils.copyProperties(item,dishDto);
            //获取菜品分类id
            Long categoryId = item.getCategoryId();
            LambdaQueryWrapper<Category> queryWrapper = new LambdaQueryWrapper<>();
            queryWrapper.eq(Category::getId,categoryId);
            //根据菜品分类id查询对应的菜品分类数据
            Category category = categoryService.getOne(queryWrapper);
            //判断菜品分类数据是否为空
            if (category != null){
                String categoryName = category.getName();
                //将菜品分类名称赋值给dishDto
                dishDto.setCategoryName(categoryName);
            }
            //获取菜品id
            Long dishId = item.getId();
            //根据菜品id获得对应关系表中的口味数据
            LambdaQueryWrapper<DishFlavor> flavorQueryWrapper = new LambdaQueryWrapper<>();
            flavorQueryWrapper.eq(DishFlavor::getDishId,dishId);
            //获得口味数据
            List<DishFlavor> dishFlavors = dishFlavorService.list(flavorQueryWrapper);

            //dishFlavors赋值给dishDto
            dishDto.setFlavors(dishFlavors);
            return dishDto;
        }).collect(Collectors.toList());

        return dishDtoList;
    }

}
