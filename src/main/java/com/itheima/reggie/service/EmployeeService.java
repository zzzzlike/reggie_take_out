package com.itheima.reggie.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.reggie.entity.Employee;

import java.util.Map;

public interface EmployeeService extends IService<Employee> {

    /**
     * 员工登录
     * @param employee
     * @return
     */
    Map checkLogin(Employee employee);

//    /**
//     * 员工新增(公共字段填充)
//     * @param id
//     * @param employee
//     * @return
//     */
//    boolean save(Long id, Employee employee);

    /**
     * 员工分页
     * @param page
     * @param pageSize
     * @param name
     * @return
     */
    Page<Employee> selectByPage(int page,int pageSize,String name);

//    /**
//     * 根据id修改员工信息(公共字段填充)
//     * @param id
//     * @param employee
//     * @return
//     */
//    boolean updateEmpById(Long id,Employee employee);


}
