package com.itheima.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.reggie.entity.AddressBook;

import java.util.List;

public interface AddressBookService extends IService<AddressBook> {

    /**
     * 新增地址
     * @param addressBook
     */
    void saveAddress(AddressBook addressBook);

    /**
     * 查询指定用户的全部地址
     * @return
     */
    List<AddressBook> getAllAddress();

    /**
     * 设置默认地址
     * @param addressBook
     */
    void updateDefault(AddressBook addressBook);

    /**
     * 根据id查询地址
     * @param id
     * @return
     */
    AddressBook selectAddressById(Long id);

    /**
     * 根据当前用户id查询默认地址
     * @return
     */
    AddressBook getDefaultAddressById();

    /**
     * 批量（单个）删除地址
     * @param ids
     */
    void deleteAddress(List<Long> ids);

    /**
     * 修改地址
     * @param addressBook
     */
    void updateAddress(AddressBook addressBook);
}
