package com.itheima.reggie.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.reggie.dto.SetmealDto;
import com.itheima.reggie.entity.Setmeal;
import com.itheima.reggie.entity.SetmealDish;

import java.util.List;

public interface SetmealService extends IService<Setmeal> {

    /**
     * 新增套餐
     * @param setmealDto
     */
    void addSetmeal(SetmealDto setmealDto);

    /**
     * 套餐分页查询
     * @param page
     * @param pageSize
     * @param name
     * @return
     */
    Page selectPage(int page,int pageSize,String name);

    /**
     * 删除套餐
     * @param ids
     */
    void deleteSetmeal(List<Long> ids);

    /**
     * 起售/停售套餐
     * @param status
     * @param ids
     */
    void updateStatus(Integer status, List<Long> ids);

    /**
     * 根据套餐id查询套餐数据（回显数据）
     * @param id
     * @return
     */
    SetmealDto getSetmealId(Long id);

    /**
     * 修改套餐
     * @param setmealDto
     */
    void updateSetmeal(SetmealDto setmealDto);

    /**
     *根据套餐分类id，查询对应套餐数据
     * @param setmeal
     * @return
     */
    List<Setmeal> selectSetmeal(Setmeal setmeal);
}
