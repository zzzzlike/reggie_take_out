package com.itheima.reggie.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.reggie.dto.DishDto;
import com.itheima.reggie.entity.Dish;

import java.util.List;

public interface DishService extends IService<Dish> {

    /**
     * 添加菜品口味
     * @param dishDto
     */
    boolean saveWithFlavor(DishDto dishDto);

    /**
     * 分页查询菜品数据
     * @param page
     * @param pageSize
     * @param name
     * @return
     */
    Page selectPage(int page,int pageSize,String name);

    /**
     * 根据id查询菜品（回显数据）
     * @param id
     * @return
     */
    DishDto selectById(Long id);

    /**
     * 修改菜品数据
     * @param dishDto
     * @return
     */
    boolean updateDish(DishDto dishDto);

    /**
     * 批量删除菜品
     * @param ids
     */
    void deleteDish(List<Long> ids);

    /**
     * 起售/停售  批量起售/批量停售
     * @param status
     * @param ids
     */
    void updateStatus(Integer status,List<Long> ids);

    /**
     * 根据菜品分类id,查询对应菜品数据
     * @param dish
     * @return
     */
    List<DishDto> selectDishList(Dish dish);
}
