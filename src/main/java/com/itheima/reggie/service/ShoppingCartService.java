package com.itheima.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.reggie.entity.ShoppingCart;

import java.util.List;

public interface ShoppingCartService extends IService<ShoppingCart> {

    /**
     * 添加购物车
     * @param shoppingCart
     * @return
     */
    ShoppingCart addShoppingCart(ShoppingCart shoppingCart);

    /**
     * 查询购物车
     * @return
     */
    List<ShoppingCart> selectShoppingCart();

    /**
     * 取消购物车
     * @param shoppingCart
     * @return
     */
    ShoppingCart subShoppingCart(ShoppingCart shoppingCart);

    /**
     * 清空购物车
     */
    void deleteShoppingCart();
}
