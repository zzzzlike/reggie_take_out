package com.itheima.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.reggie.entity.User;

import java.util.Map;


public interface UserService extends IService<User> {

    /**
     *发送手机短信验证码
     * @param user
     * @return
     */
    Map sendPhoneMsg(User user);

    /**
     * 用户输入验证码,进行登录
     * @param phone
     * @param code
     * @param codeSession
     * @return
     */
    Map userLogin(String phone,String code,String codeSession);
}
