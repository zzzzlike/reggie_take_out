package com.itheima.reggie.controller;

import com.alibaba.excel.EasyExcel;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.reggie.common.R;
import com.itheima.reggie.entity.Orders;
import com.itheima.reggie.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Slf4j
@RestController
@RequestMapping("/order")
public class OrderController {

    @Autowired
    OrderService orderService;

    /**
     * 下单
     * @param orders
     * @return
     */
    @PostMapping("/submit")
    public R<String> submit(@RequestBody Orders orders){
        orderService.submitOrders(orders);
        return R.success("下单成功!");
    }

    /**
     * 分页查询订单
     * @param page
     * @param pageSize
     * @return
     */
    @GetMapping("/userPage")
    public R<Page> page(int page,int pageSize){
        Page<Orders> pageInfo = orderService.selectPage(page, pageSize);
        return R.success(pageInfo);
    }

    /**
     * 根据条件查询订单
     * @param map
     * @return
     */
    @GetMapping("/page")
    public R<Page> selectOrdersCondition(@RequestParam Map map){
        Page pageInfo = orderService.OrdersByCondition(map);
        return R.success(pageInfo);
    }

    /**
     * 订单派送
     * @param orders
     * @param request
     * @return
     */
    @PutMapping
    public R<String> updateOrderStatus(@RequestBody Orders orders, HttpServletRequest request){
        log.info("订单派送:",orders.toString());

        Long ordersId = (Long) request.getSession().getAttribute("orders");
        orders.setCheckoutTime(LocalDateTime.now());
        orders.setUserId(ordersId);
        orderService.updateById(orders);

        return R.success("派送成功");
    }


    /**
     * Excel导出订单数据
     * @param beginTime
     * @param endTime
     */
    @GetMapping("/export")
    public void exportExcel(String beginTime, String endTime, HttpServletResponse response) throws IOException {
        // 这里注意 有同学反应使用swagger 会导致各种问题，请直接用浏览器或者用postman
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setCharacterEncoding("utf-8");

        // 这里URLEncoder.encode可以防止中文乱码 当然和easyexcel没有关系
//        String fileName = URLEncoder.encode("测试", "UTF-8").replaceAll("\\+", "%20");
        String fileName = UUID.randomUUID().toString();

        response.setHeader("Content-disposition", "attachment;filename*=utf-8''" + fileName + ".xlsx");

        LambdaQueryWrapper<Orders> wrapper = new LambdaQueryWrapper<>();
        //select * from orders where create_time between ? and ?;
        wrapper.between(StringUtils.isNotEmpty(beginTime) && StringUtils.isNotEmpty(endTime),  //动态SQL
                Orders::getOrderTime,
                beginTime,
                endTime);
        List<Orders> list = orderService.list(wrapper);
        EasyExcel.write(response.getOutputStream(), Orders.class).sheet("模板").doWrite(list);
    }
}
