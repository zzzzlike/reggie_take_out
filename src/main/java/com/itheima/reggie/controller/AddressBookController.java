package com.itheima.reggie.controller;

import com.itheima.reggie.common.R;
import com.itheima.reggie.entity.AddressBook;
import com.itheima.reggie.service.AddressBookService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/addressBook")
public class AddressBookController {

    @Autowired
    AddressBookService addressBookService;

    /**
     * 新增地址
     * @param addressBook
     * @return
     */
    @PostMapping
    public R<String> save(@RequestBody AddressBook addressBook){

        addressBookService.saveAddress(addressBook);
        return R.success("新增地址成功！");
    }

    /**
     * 查询指定用户的全部地址
     * @return
     */
    @GetMapping("/list")
    public R<List<AddressBook>> getAllAddressBook(){
        List<AddressBook> allAddress = addressBookService.getAllAddress();
        return R.success(allAddress);
    }

    /**
     * 设置默认地址
     * @param addressBook
     * @return
     */
    @PutMapping("/default")
    public R<String> updateDefaultAddress(@RequestBody AddressBook addressBook){
        addressBookService.updateDefault(addressBook);
        return R.success("设置默认地址成功！");
    }

    /**
     * 根据ID查询地址
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public R<AddressBook> getById(@PathVariable Long id){
        AddressBook addressBook = addressBookService.selectAddressById(id);
        if (addressBook == null){
            return R.error("未查询到地址");
        }else {
            return R.success(addressBook);
        }
    }

    /**
     * 根据当前用户id查询默认地址
     * @return
     */
    @GetMapping("/default")
    public R<AddressBook> getDefaultAddress(){
        AddressBook addressBook = addressBookService.getDefaultAddressById();
        if (addressBook == null){
            return R.error("未查到默认地址！");
        }
        return R.success(addressBook);
    }

    /**
     * 批量（单个）删除地址
     * @param ids
     * @return
     */
    @DeleteMapping
    public R deleteAddressBooks(@RequestParam List<Long> ids){
        addressBookService.deleteAddress(ids);
        return R.success("删除地址成功！");
    }

    /**
     * 修改地址
     * @param addressBook
     * @return
     */
    @PutMapping
    public R<String> updateAddressBooks(@RequestBody AddressBook addressBook){
        addressBookService.updateAddress(addressBook);
        return R.success("修改地址成功！");
    }
}
