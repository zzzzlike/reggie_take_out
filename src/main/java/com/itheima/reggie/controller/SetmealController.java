package com.itheima.reggie.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.reggie.common.R;
import com.itheima.reggie.dto.SetmealDto;
import com.itheima.reggie.entity.Setmeal;
import com.itheima.reggie.service.SetmealDishService;
import com.itheima.reggie.service.SetmealService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/setmeal")
public class SetmealController {

    @Autowired
    SetmealService setmealService;

    @Autowired
    SetmealDishService setmealDishService;

    /**
     * 新增套餐
     * @param setmealDto
     * @return
     */
    @CacheEvict(value = "setmealDto_cache",key ="'setmealDto_'+#setmealDto.categoryId")
    @PostMapping
    public R<String> saveSetmeal(@RequestBody SetmealDto setmealDto){
        setmealService.addSetmeal(setmealDto);
        return R.success("新增套餐成功！");
    }

    /**
     * 套餐分页查询
     * @param page
     * @param pageSize
     * @param name
     * @return
     */
    @GetMapping("/page")
    public R<Page> page(int page,int pageSize,String name){
        Page pageInfo = setmealService.selectPage(page, pageSize, name);
        return R.success(pageInfo);
    }

    /**
     * 删除套餐
     * @param ids
     * @return
     */
    @DeleteMapping
    public R deleteBySetmeal(@RequestParam List<Long> ids){
        //TODO
        setmealService.deleteSetmeal(ids);
        return R.success("删除套餐成功！");
    }

    /**
     * 起售/停售套餐
     * @param status
     * @param ids
     * @return
     */
    @CacheEvict(value = "setmeal_cache",allEntries = true)
    @PostMapping("/status/{status}")
    public R updateByStatus(@PathVariable Integer status,@RequestParam List<Long> ids){
        setmealService.updateStatus(status,ids);
        return R.success("修改套餐状态成功！");
    }

    /**
     * 根据套餐id查询套餐数据（回显数据）
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public R getBySetmealId(@PathVariable Long id){
        SetmealDto setmealDto = setmealService.getSetmealId(id);
        return R.success(setmealDto);

    }
    /**
     * 修改套餐
     * @param setmealDto
     * @return
     */
    @PutMapping
    public R<String> updateBySetmeal(@RequestBody SetmealDto setmealDto){
        setmealService.updateSetmeal(setmealDto);
        return R.success("修改套餐成功！");
    }

    /**
     *根据套餐分类id，查询对应套餐数据
     * @param setmeal
     * @return
     */
    @Cacheable(value = "setmeal_cache",key ="'setmeal_'+#setmeal.categoryId",unless = "#result==null")
    @GetMapping("/list")
    public R<List<Setmeal>> selectBySetmeal(Setmeal setmeal){
        List<Setmeal> setmealList = setmealService.selectSetmeal(setmeal);
        return R.success(setmealList);
    }

}
