package com.itheima.reggie.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.reggie.common.R;
import com.itheima.reggie.dto.DishDto;
import com.itheima.reggie.entity.Dish;
import com.itheima.reggie.service.DishService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/dish")
public class DishController {

    @Autowired
    DishService dishService;

    @Autowired
    RedisTemplate redisTemplate;

    /**
     * 新增菜品
     * @param dishDto
     * @return
     */
    @PostMapping
    public R<String> save(@RequestBody DishDto dishDto){
        log.info(dishDto.toString());
        dishService.saveWithFlavor(dishDto);
        //清理所有菜品的缓存数据
//        Set keys = redisTemplate.keys("dish_*"); //获取所有以dish_xxx开头的key
//        redisTemplate.delete(keys); //删除这些key
        //清理某个分类下面的菜品缓存数据
        String key = "dish_" + dishDto.getCategoryId() + "_1";
        redisTemplate.delete(key);

        return R.success("新增菜品成功！");

    }

    /**
     * 分页查询菜品数据
     * @param page
     * @param pageSize
     * @param name
     * @return
     */
    @GetMapping("/page")
    public R<Page> selectByPage(int page,int pageSize,String name){
        Page pageInfo = dishService.selectPage(page, pageSize, name);
        return R.success(pageInfo);
    }


    /**
     * 根据id查询菜品（回显数据）
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public R getByDishId(@PathVariable Long id){
        DishDto dishDto = dishService.selectById(id);
        return R.success(dishDto);
    }

    /**
     * 修改菜品数据
     * @param dishDto
     * @return
     */
    @CacheEvict(value = "dishDto_cache",key = "'dishDto_'+#dishDto.categoryId")
    @PutMapping
    public R<String> updateByDish(@RequestBody DishDto dishDto){
        boolean flag = dishService.updateDish(dishDto);
        if (flag){
            return R.success("修改菜品数据成功！");
        }else {
            return R.error("修改菜品数据失败！");
        }
    }

    /**
     * 批量（单个）删除菜品
     * @param ids
     * @return
     */
    @DeleteMapping
    public R deleteDish(@RequestParam List<Long> ids){
        dishService.deleteDish(ids);
        return R.success("删除成功");
    }

    /**
     * 起售/停售  批量起售/批量停售
     * @param status
     * @param ids
     * @return
     */
    @PostMapping("/status/{status}")
    public R updateByStatus(@PathVariable Integer status,@RequestParam List<Long> ids){

        dishService.updateStatus(status,ids);
        return R.success("操作成功！");
    }

    /**
     * 根据菜品分类id,查询对应菜品数据
     * @param dish
     * @return
     */
    @GetMapping("/list")
    public R<List<DishDto>> selectByDish( Dish dish){
        log.info(dish.toString()+"===================");
        List<DishDto> dishDtoList = dishService.selectDishList(dish);
        return R.success(dishDtoList);
    }

}
