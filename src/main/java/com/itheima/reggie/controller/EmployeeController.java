package com.itheima.reggie.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.reggie.common.R;
import com.itheima.reggie.entity.Employee;
import com.itheima.reggie.service.EmployeeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping("/employee")
public class EmployeeController {

    @Autowired
    EmployeeService employeeService;

    /**
     * 员工登录
     * @param request
     * @param employee
     * @return
     */
    @PostMapping("/login")
    public R login(HttpServletRequest request, @RequestBody Employee employee){
        log.info("用户登录:{}",employee);

        //调用业务层验证用户登录的方法
        Map loginResultMap = employeeService.checkLogin(employee);
        Employee loginEmp = (Employee) loginResultMap.get("employee");

        //返回登录结果
        if (loginEmp == null){
            return R.error((String) loginResultMap.get("msg"));
        }

        //用户登录成功后,将用户信息存储在session中
        request.getSession().setAttribute("employee",loginEmp.getId());

        return R.success(loginEmp);
    }

    /**
     * 员工退出
     * @param
     * @return
     */
    @PostMapping("/logout")
    public R logout(HttpServletRequest request){
        //用户成功之后,将用户信息存储到session中
        //用户退出之后,将存储到session中的用户信息删除
        request.getSession().removeAttribute("employee");
        return R.success("null");
    }

    /**
     * 员工新增
     * @param request
     * @param employee
     * @return
     */
    @PostMapping
    public R<String> addEmployee(HttpServletRequest request, @RequestBody Employee employee) {
        log.info("新增员工,员工信息:{}", employee.toString());
//        //获取当前用户的id
//        Long id = (Long) request.getSession().getAttribute("employee");
        //初始密码123456,md5加密
        employee.setPassword(DigestUtils.md5DigestAsHex("123456".getBytes()));
        //调用业务层保存 employee数据
        boolean flag = employeeService.save(employee);
        //判断
        //返回结果R
        if (flag){
            return R.success("添加成功");
        }else {
            return R.error("添加失败");
        }
    }

    /**
     * 员工分页查询
     * @param page
     * @param pageSize
     * @param name
     * @return
     */
    @GetMapping("/page")
    public R<Page> page(int page,int pageSize,String name){
        Page<Employee> employeePage = employeeService.selectByPage(page, pageSize, name);
        return R.success(employeePage);
    }

    /**
     * 根据id修改员工信息
     * @param request
     * @param employee
     * @return
     */
    @PutMapping
    public R<String> updateByStatus(HttpServletRequest request, @RequestBody Employee employee){
        log.info("修改员工状态,员工信息:{}", employee.toString());

//        //从session中获取员工id
//        Long id = (Long) request.getSession().getAttribute("employee");
        //3、调用业务层修改员工信息
        boolean flag = employeeService.updateById(employee);
        if (flag){
            return R.success("修改成功！");
        }else {
            return R.error("修改失败！");
        }
    }

    /**
     * 根据id查询员工（回显数据）
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public R<Employee> getByEmpId(@PathVariable Long id){
        Employee emp = employeeService.getById(id);

        if (emp != null){
            return R.success(emp);
        }else {
            return R.error("未查到此员工,查询失败！");
        }

    }
}
