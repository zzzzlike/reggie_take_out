package com.itheima.reggie.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.reggie.common.R;
import com.itheima.reggie.entity.Category;
import com.itheima.reggie.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/category")
public class CategoryController {

    @Autowired
    CategoryService categoryService;

    /**
     * 新增分类
     * @param category
     * @return
     */
    @PostMapping
    public R<String> save(@RequestBody Category category){
        categoryService.save(category);
        return R.success("新增分类成功!");
    }

    /**
     * 分类分页查询
     * @param page
     * @param pageSize
     * @return
     */
    @GetMapping("/page")
    public R<Page> page(int page,int pageSize){
        Page<Category> categoryPage = categoryService.selectByPage(page, pageSize);
        return R.success(categoryPage);
    }

    /**
     * 根据id删除分类
     * @param id
     * @return
     */
    @DeleteMapping
    public R<String> delete(Long id){
        boolean flag = categoryService.deleteById(id);
        if(flag){
            return R.success("删除分类成功！");
        }else {
            return R.error("删除分类失败！");
        }
    }

    /**
     * 根据id修改分类
     * @param category
     * @return
     */
    @PutMapping
    public R<String> update(@RequestBody Category category){
        boolean flag = categoryService.updateById(category);
        if (flag){
            return R.success("修改成功！");
        }else {
            return R.error("修改失败！");
        }
    }

    /**
     * 根据type查询所有分类
     * @param category
     * @return
     */
    @GetMapping("/list")
    public R<List<Category>> select(Category category){
        List<Category> byType = categoryService.getByType(category);
        return R.success(byType);
    }
}
