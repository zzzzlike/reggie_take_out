package com.itheima.reggie.controller;

import com.itheima.reggie.common.R;
import com.itheima.reggie.entity.User;
import com.itheima.reggie.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Slf4j
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    UserService userService;

    @Autowired
    RedisTemplate redisTemplate;

    /**
     * 发送手机验证码
     * @param user
     * @param request
     * @return
     */
    @PostMapping("/sendMsg")
    public R<String> sendMsg(@RequestBody User user, HttpServletRequest request){
        //获取用户输入的手机号和验证码
        Map map = userService.sendPhoneMsg(user);
        Map codeMap = (Map) map.get("codeMap");
        if (codeMap == null){
            return R.error((String) map.get("msg"));
        }
        //codeMap不为空,从codeMap取数据;
        String phone = codeMap.get("phone").toString();
        String code = codeMap.get("code").toString();
        log.info("手机验证码为:{}",code);

        //将code存到session中
//        request.getSession().setAttribute(phone,code);

        //需要将生成的验证码保存到Redis,设置过期时间
        redisTemplate.opsForValue().set(phone,code,1, TimeUnit.MINUTES);

        return R.success("发送验证码成功！");
    }

    /**
     * 用户输入验证码,进行登录
     * @param map
     * @param request
     * @return
     */
    @PostMapping("/login")
    public R<User> login(@RequestBody Map map,HttpServletRequest request){
        //获取用户登录输入的手机号和验证码
        String phone = map.get("phone").toString();
        String code = map.get("code").toString();
        //获取session中的验证码
//        String codeSession = request.getSession().getAttribute(phone).toString();

        //从Redis中获取缓存的验证码
        Object codeInSession = redisTemplate.opsForValue().get(phone);

        Map userLoginMap = userService.userLogin(phone, code, (String) codeInSession);
        User user = (User) userLoginMap.get("user");

        //判断用户是否存在
        if (user == null){
            return R.error((String) map.get("msg"));
        }
        //登录成功将用户id存储到session中
        Long userId = user.getId();
        request.getSession().setAttribute("user",userId);

        //登录成功删除Redis里面的验证码
        redisTemplate.delete(phone);
        //返回用户信息
        return R.success(user);
    }

    /**
     * 退出登录
     * @param request
     * @return
     */
    @PostMapping("/loginout")
    public R<String> loginOut(HttpServletRequest request){
        request.getSession().removeAttribute("user");
        return R.success("退出成功！");

    }
}
